package com.projectcloud;

import java.util.Objects;

public class BankAccount {

    private Long id;
    private String surname;
    private String firstname;
    private long balance;
    private String risk;

    public BankAccount(String surname, String firstname, long balance, String risk, long id) {
        this.surname = surname;
        this.firstname = firstname;
        this.balance = balance;
        this.risk = risk;
        this.id = id;
    }

    public BankAccount() {
    }

    public Long getId() {
        return this.id;
    }

    public String getSurname() {
        return surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public long getBalance() {
        return balance;
    }

    public String getRisk() {
        return risk;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.surname, this.firstname, this.balance);
    }

    @Override
    public String toString() {
        return "com.projectcloud.checkaccount.BankAccount{" + "id=" + getId() + ", surname='" + getSurname() + ", firstname'" + getFirstname() + '\'' + ", balance='" + getBalance() + ", risk='" + getRisk() + '\'' + '}';
    }
}
