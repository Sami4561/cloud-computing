package com.projectcloud;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;

@Path("getrisk")
public class CheckAccountService {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getRisk(@QueryParam("id") Long id) {
        BankAccount bankAccount = ClientBuilder.newClient().target("https://accmanager-service.ew.r.appspot.com/accounts/")
                .path(String.valueOf(id))
                .request(MediaType.APPLICATION_JSON)
                .get(BankAccount.class);
        return bankAccount.getRisk();
    }
}
