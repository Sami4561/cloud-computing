<?php
// Client simulant une demande de prêt de plus de 10 000 euros avec un bas taux de risque et étant ACCEPTED dans AppManager
// La réponse attendue est donc APPROVED

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

$client = new Client([
    'base_uri' => 'https://loanapproval-service.ew.r.appspot.com',
]);

$response = $client->request('GET', 'loan/5636645067948032/Alonso/25000', ['verify' => true]);
$message = $response->getBody();
echo "Alonso: ".$message."\n";