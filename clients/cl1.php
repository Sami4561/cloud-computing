<?php
// Client simulant une demande de prêt de moins de 10 000 euros avec un bas un taux de risque
// La réponse attendue est donc APPROVED

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

$client = new Client([
    'base_uri' => 'https://loanapproval-service.ew.r.appspot.com',
]);

$response = $client->request('GET', 'loan/5636645067948032/Bolt/2500', ['verify' => true]);
$message = $response->getBody();
echo "Bolt: ".$message."\n";