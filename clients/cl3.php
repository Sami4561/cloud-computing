<?php
// Client simulant une demande de prêt de plus de 10 000 euros avec un haut un taux de risque et étant REFUSED dans AppManager
// La réponse attendue est donc REFUSED

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

$client = new Client([
    'base_uri' => 'https://loanapproval-service.ew.r.appspot.com',
]);

$response = $client->request('GET', 'loan/5646488461901824/Zidane/35000', ['verify' => true]);
$message = $response->getBody();
echo "Zidane: ".$message."\n";