package com.projectcloud;

import java.util.Objects;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;

@Entity(name = "accounts")
public class BankAccount {

    @Id
    private Long id;
    private String surname;
    private String firstname;
    private long balance;
    private Risk risk;

    BankAccount() {}

    BankAccount(String surname, String firstname, long balance, Risk risk) {
        this.surname = surname;
        this.firstname = firstname;
        this.balance = balance;
        this.risk = risk;
    }

    public Long getId() {
        return this.id;
    }

    public String getSurname() {
        return surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public long getBalance() {
        return balance;
    }

    public Risk getRisk() {
        return risk;
    }

    public void creditAccount(Long toCredit) {
        balance += toCredit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.surname, this.firstname, this.balance);
    }

    @Override
    public String toString() {
        return "BankAccount{" + "id=" + getId() + ", surname='" + getSurname() + ", firstname'" + getFirstname() + '\'' + ", balance='" + getBalance() + ", risk='" + getRisk().toString() + '\'' + '}';
    }
}
