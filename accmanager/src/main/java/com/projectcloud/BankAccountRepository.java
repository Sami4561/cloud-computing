package com.projectcloud;

import java.util.List;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

public interface BankAccountRepository extends DatastoreRepository<BankAccount, Long> {
    List<BankAccount> findBySurname(String name);

    List<BankAccount> findByBalanceGreaterThan(long balance);

    List<BankAccount> findByRisk(String risk);
}
