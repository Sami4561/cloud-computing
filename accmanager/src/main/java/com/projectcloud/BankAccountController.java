package com.projectcloud;

import org.springframework.web.bind.annotation.*;

@RestController
public class BankAccountController {

    private final BankAccountRepository repository;

    BankAccountController(BankAccountRepository repository) {
        this.repository = repository;
    }

    // Tous les comptes
    @GetMapping("/accounts")
    Iterable<BankAccount> all() {
        return repository.findAll();
    }

    // Ajouter un compte
    @PostMapping(value= "/accounts", consumes = "application/json")
    BankAccount newBankAccount(@RequestBody BankAccount newBankAccount) {
        return repository.save(newBankAccount);
    }

    // Un seul compte selon id
    @GetMapping("/accounts/{id}")
    BankAccount getOne(@PathVariable Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new BankAccountNotFoundException(id));
    }

    // Somme supérieure à un certain montant
    @GetMapping("/accountb/{balance}")
    Iterable<BankAccount> getByBalanceGreaterThan(@PathVariable Long balance) {
        return repository.findByBalanceGreaterThan(balance);
    }

    // Créditer un montant sur un compte
    @GetMapping("/accounts/{id}/{toCredit}")
    Long creditAccount(@PathVariable Long id, @PathVariable Long toCredit) {
        BankAccount account = repository.findById(id).orElseThrow(() -> new BankAccountNotFoundException(id));
        account.creditAccount(toCredit);
        repository.save(account);
        return account.getBalance();
    }

    // Supprimer un compte
    @DeleteMapping("/accounts/{id}")
    void deleteBankAccount(@PathVariable Long id) {
        repository.deleteById(id);
    }

    // À SUPPRIMER PLUS TARD
    @GetMapping("/checkaccount/{id}")
    String checkAccount(@PathVariable Long id) {
        BankAccount account = repository.findById(id)
                .orElseThrow(() -> new BankAccountNotFoundException(id));
        return account.getRisk().toString();
    }
}