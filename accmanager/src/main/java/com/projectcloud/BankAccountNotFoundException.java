package com.projectcloud;

class BankAccountNotFoundException extends RuntimeException {

    BankAccountNotFoundException(Long id) {
        super("Could not find bank account " + id);
    }
}