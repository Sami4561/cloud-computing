package com.proejctcloud.loanapproval;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoanapprovalApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoanapprovalApplication.class, args);
	}

}
