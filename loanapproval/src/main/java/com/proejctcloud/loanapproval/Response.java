package com.proejctcloud.loanapproval;

public enum Response {
    APPROVED,
    REFUSED
}
