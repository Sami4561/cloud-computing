package com.proejctcloud.loanapproval;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class LoanApprovalController {

    private final RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/loan/{id}/{name}/{amount}")
    public String loanApproval(@PathVariable Long id, @PathVariable String name, @PathVariable Long amount) {
        ResponseEntity<String> risk;
        ResponseEntity<Long> newBalance;
        boolean isApproved = false;
        // On vérifie d'abord le montant demandé
        if (amount < 10000) {
            risk = callCheckAccount(id);
           if (risk.getBody().equals("LOW")) { // On peut créditer la somme directement
               newBalance = callAccManager(id, amount);
               if (verifyResponseIsOk(newBalance)) {
                   return Response.APPROVED.toString();
               }
           }
        }
        // On est dans le cas où le montant demandé est supérieur à 10 000 euros ou si le risque est élevé
        isApproved = callAppManager(name).getBody().equals("ACCEPTED");
        if (isApproved) {
            newBalance = callAccManager(id, amount);
            if (verifyResponseIsOk(newBalance)) {
                return Response.APPROVED.toString();
            }
        }
        return Response.REFUSED.toString();
    }

    /**
     * Appelle le service CheckAccount qui appelle AccManager pour récupérer le risque sur un compte
     * @param id du compte à regarder
     * @return String contenant le risque, HIGH ou LOW
     */
    public ResponseEntity<String> callCheckAccount(Long id){
        String checkAccount = "https://checkaccount-service.herokuapp.com/getrisk?id=";
        return restTemplate.getForEntity(checkAccount + id, String.class);
    }

    /**
     * Appelle le service AccManager pour créditer un montant sur un compte
     * @param id du compte à créditer
     * @param amount à créditer
     * @return String contenant le risque, HIGH ou LOW
     */
    public ResponseEntity<Long> callAccManager(Long id, Long amount){
        String accManager = "https://accmanager-service.ew.r.appspot.com/accounts";
        return restTemplate.getForEntity(accManager + "/" + id + "/" + amount, Long.class);
    }

    /**
     * Appelle le service AppManager pour savoir si le prêt peut être accordé avec un risque élevé ou une somme supérieure à 10 000 euros
     * @param name du compte à regarder
     * @return String contenant la réponse, ACCEPTED ou REFUSED
     */
    public ResponseEntity<String> callAppManager(String name){
        String appManager = "https://appmanager-service.herokuapp.com/approvals";
        return restTemplate.getForEntity(appManager + "/isApproved" + "/" + name, String.class);
    }

    /**
     * Vérifier qu'une requête renvoie bien HTTP 200
     * @param response la réponse à analyser
     * @return boolean à false si code différent de 200
     */
    public boolean verifyResponseIsOk(ResponseEntity<Long> response) {
        return response.getStatusCode().equals(HttpStatus.OK);
    }
}
