package com.projectcloud;

class ApprovalNotFoundException extends RuntimeException {

    ApprovalNotFoundException(String name) {
        super("Could not find any approvals for " + name);
    }
}