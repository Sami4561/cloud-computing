package com.projectcloud;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;

import javax.persistence.NoResultException;
import javax.validation.Valid;
import java.util.List;

@ExecuteOn(TaskExecutors.IO)
@Controller("approvals")
public class ApprovalController {
    private final ApprovalService service;

    public ApprovalController (ApprovalService service) {
        this.service = service;
    }

    @Get
    public List<Approval> findAll() {
        return service.findAll();
    }

    @Get("/{name}")
    public Approval getOne(String name) {
        try {
            return service.findByName(name);
        } catch (NoResultException noResult) {
            throw new ApprovalNotFoundException(name);
        }
    }

    @Get("isApproved/{name}")
    public String isApproved(String name) {
        Approval approval;
        try {
            approval = service.findByName(name);
        } catch (NoResultException noResult) {
            throw new ApprovalNotFoundException(name);
        }
        return approval.getResponse().toString();
    }

    @Post
    public HttpResponse<Approval> save(@Body @Valid Approval approval) {
        return HttpResponse.created(service.save(approval));
    }

    @Delete("/{id}")
    public HttpResponse delete (Long id) {
        service.deleteById(id);
        return HttpResponse.noContent();
    }


}
