package com.projectcloud;

import io.micronaut.transaction.annotation.ReadOnly;
import jakarta.inject.Singleton;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Singleton
public class ApprovalService {
    private final EntityManager entityManager;

    public ApprovalService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @ReadOnly
    public Optional<Approval> findById(@NotNull Long id){
        return Optional.ofNullable(entityManager.find(Approval.class, id));
    }

    @ReadOnly
    public Approval findByName(@NotNull String name) {
        TypedQuery<Approval> query = entityManager.createQuery("FROM Approval AS a WHERE a.name= :name", Approval.class).setParameter("name", name);
        if (query.getResultList().size() > 1) {
            return query.getResultList().get(0);
        }
        return query.getSingleResult();
    }

    @Transactional
    public Approval save(Approval approval) {
        entityManager.persist(approval);
        return approval;
    }

    @ReadOnly
    public List<Approval> findAll() {
        TypedQuery<Approval> query = entityManager.createQuery("FROM Approval as a order by a.name", Approval.class);
        return query.getResultList();
    }

    @Transactional
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(entityManager::remove);
    }


}
