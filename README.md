# Projet Cloud Computing 
## Ambroise GABERT et Sami BÉHAR

AccManager : service Springboot déployé sur GAE avec BDD Datastore  
AppManager : service Micronaut déployé sur Heroku avec BDD PostgreSQL  
CheckAccount : service Jersey déployé sur Heroku  
LoanApproval : service Springboot déplyoé sur GAE  

## Répartition

Ambroise :
- Service CheckAccount
- Clients

Sami :
- Service AccManager
- Service AppManager
- Service LoanApproval

## URLs : 
- [AccManager](https://accmanager-service.ew.r.appspot.com/accounts)
- [AppManager](https://appmanager-service.herokuapp.com/approvals) 
- [CheckAccount](https://checkaccount-service.herokuapp.com/getrisk?id=5646488461901824)
- [LoanApproval](https://loanapproval-service.ew.r.appspot.com/loan/5636645067948032/Bolt/1000) (ex : prêt de 1000 euros, risque faible)


Les trois clients sont dans le dossier clients/. Le script launch.sh lance une vingtaine d'instance de clients en tout.  
Les logs à la suite de cet appel se trouvent dans le dossiers clients/logs/.

## Effectuer une demande de prêt

En spécifiant les paramètres désirés, appeler le service LoanApproval avec cette URL : 
https://loanapproval-service.herokuapp.com/loan/id_compte/nom_compte/montant

Dans le cas où le prêt est accordé, le message APPROVED est renvoyé (et le montant crédité sur le compte), sinon le message REFUSED est renvoyé.